<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

</head>
<body class="bg-light">
    <?php
    // echo "Hello World \"hi\""."<br>";
    // echo 'i\'m a student<br>';
    // ?>    

    <?php
    // $foo = 10;   // $foo is an integer
    // $bar = (boolean) $foo;   // $bar is a boolean
    // echo $bar;

    // echo "<br>";

    // $a = 10;
    // $b = (int)20.2;

    // echo $a + $b."<br>";

    // $name1 = "fahim";
    // $name2 = "fahim <br>";

    // echo "$name1 $name2 <br>";

    $text = <<<TEXT



    <div class="container card-body p-5 shadow-sm p-3 mb-5 bg-white rounded ">
      <p class="card-text">Hi there,<br><br>
      Sometimes you just want to send a simple HTML email with a simple design and<br>
      clear call to action. this is it.<br>
      <br></p>
      <a href="#" class="btn btn-primary">Call to Action</a>
      <br>
      <br>
      this is a really simple email template. its sole purpose is to get recipient to click<br>
      the button with no distractions.<br>
      <br>
      Good luck! Hope it works
    </div>



    TEXT;
    echo $text

?>

</body>
</html>