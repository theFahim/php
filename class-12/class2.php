<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>class 2</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <style>
        .header, footer{
            width:  100%;
            height: 80px;
            background-color: blue;
        }
        .main{
            width:  100%;
            height: 100vh;
            background-color: red;
        }
    </style>

</head>
<body>
    <div class="header">

    </div>
    <div class="main">

    </div>
    <footer>

    </footer>
</body>
</html>