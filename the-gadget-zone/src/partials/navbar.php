<?php
    include_once($_SERVER['DOCUMENT_ROOT']."/SEIP-B5-Fahim/PHP/the-gadget-zone/vendor/autoload.php");
    use Gadget\Classes\Navbar;

    $navigation = new Navbar();

    $navigation->navItems = ['Desktoppppp'=>'#desktop.php','Laptops'=>'#laptops.php','Components'=>'#components.php','Accessories'=>'#accesscories.php','Monitor'=>'#monitor.php','UPS'=>'#ups.php','Tablet'=>'#tablet.php','Camera'=>'#camera.php','Gaming'=>'#gaming.php','Pre-build'=>'#prebuild.php','Printer'=>'#printer.php'];


?>
<nav class="navbar navbar-expand-sm  justify-content-center " id="navbar">
      <ul class="navbar-nav">
  <?php foreach($navigation->navItems as $navs =>$link):?>
        <li class="nav-item ">
          <a class="nav-link" href="<?=$link?>"><?=$navs?></a>
        </li>
    <?php endforeach?>

    </nav>