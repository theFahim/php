<?php

namespace Gadget\partials;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="src/partials/css/home.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

</head>
<body>

    <!-- Header -->
    <header id="header">
        <div class="container-fluid bg-dark">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark mx-5">
                <div class="container-fluid ms-5">
                        <a href="/index.php"><img src="/src/partials/images/logo.png'" alt="logo" width="60px"></a>
                        <a class="navbar-brand" href="/index.php">Gadget Zone</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse me-5" id="navbarSupportedContent">
                        <form class="d-flex flex-grow-1 me-3">
                            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-primary text-white" type="submit">Search</button>
                        </form>
                        <button class="btn btn-outline-primary me-3 text-white">PC Builder</button>
                        <ul class="navbar-nav ms-auto mb-lg-0 ">

                            <li class="nav-item d-flex">
                                <a class="nav-link text-white" href="customer-dashboard.html"><i class="bi bi-person"></i></a>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb pt-4 pe-3">
                                      <li class="breadcrumb-item">
                                        <a href="./customerLogin/login.html" class="text-decoration-none text-white">Login</a>
                                      </li>
                                      <li class="breadcrumb-item">
                                        <a href="/customerLogin/register.html" class="text-decoration-none text-white">Register</a>
                                      </li>
                                    </ol>
                                  </nav>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="cart.html"><i class="bi bi-cart-dash-fill"></i><span>Cart Items</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>