<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mail Template</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
</head>
<body class="bg-light">
    <?php

        $header = <<<HEADER

                <div class="container ">
                <div class="container shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="container title-holder">
                        <div class="container-fluid py-2">
                            <p class="text-white text-center"><i class="bi bi-stop-circle-fill px-2"></i><i class="bi bi-mic-fill px-2"></i>
                                <i class="bi bi-play-btn-fill px-2"></i><i class="bi bi-speaker-fill px-2"></i>
                                <i class="bi bi-aspect-ratio-fill px-2"></i><i class="bi bi-camera px-2"></i></p>
                            <h3 class="text-center text-white">Welcome to 5th International Film festival </h3>
                            <p class="text-white text-center"><i class="bi bi-skip-backward-fill px-2"></i><i class="bi bi-camera-reels-fill px-2"></i>
                                <i class="bi bi-film px-2"></i><i class="bi bi-camera-fill px-2"></i></i>
                                <i class="bi bi-card-image px-2"></i><i class="bi bi-skip-forward-fill px-2"></i></p>
                        </div>
                    </div>
                </div>

                <div class="mx-5">
                    <p>Hi, Mr. John Doe</p>
                    <p class="py-3">
                        We're Exited to annouce you that Our Yearly Film Festival is taking place at xyz, Dhaka , Bangladesh. at hh:mm time at dd-mm-yy date . <br>
                        Our Organizing Comittee has put togather some amazing stuff that you can enjoy besides meeting some of the briliant filmmakers of the country.<br>
                        The last day of the Registration is dd-mm-yy so don't miss out the awesome thing we've put togather. Click the <a href="#">link</a> to register and take your limited access early <br>

                        <h5 >We'll Enjoy few Awarded Movies Togather </h5>

                        <div class="row">
                            <div class="col-4">
                            <img src="https://wallpapercave.com/wp/wp10433912.jpg" alt="" width="100%">
                            <p class="text-muted">- The Northman (2022)</p>
                            </div>
                            <div class="col-4">
                            <img src="https://wallpaperaccess.com/full/5030161.jpg" alt="" width="100%" height="76%">
                            <p class="text-muted">- Seven Samurai (1954)</p>
                            </div>
                            <div class="col-4">
                            <img src="https://wallpaperaccess.com/full/1424405.jpg" alt="" width="100%" height="76%">
                            <p class="text-muted">- The Good, The Bad and the Ugly (1966)</p>
                            </div>
                        </div>

                        <div class="container text-center">
                        <h5 class="pt-3">Foods and snacks will be served </h5>
                        <img class="justify-content-center" src="https://wallpaperaccess.com/full/1891213.jpg" alt="" width="90%">
                        <h5 class="pt-3">Briliant Shrotfilms by Country's renowned Directors will be displayed  </h5>
                        <img src="https://cdn.wallpapersafari.com/45/7/SejK9L.jpg" alt="" width="90%">
                        </div>
                    </p>
                    <button type="button" class="btn btn-outline-dark mb-4 form-control"> Register Here</button>
                </div>

                <div>
                    <div class="container text-center">Contact Us</div>
                    <div class="row pt-3">
                        
                        <div class="col text-center">
                        <a class="text-decoration-none text-dark" href="#">
                        <i class="bi bi-envelope-fill"></i>
                        <p>example.mail.com</p>
                        </a>
                        </div>
                        <div class="col text-center">
                        <a class="text-decoration-none text-dark" href="#">
                        <i class="bi bi-facebook"></i>
                        <p>Facebook</p>
                        </a>
                        </div>
                        <div class="col text-center">
                        <a class="text-decoration-none text-dark" href="#">
                        <i class="bi bi-twitter"></i>
                        <p>Twitter</p>
                        </a>
                        </div>
                        <div class="col text-center">
                        <a class="text-decoration-none text-dark" href="#">
                        <i class="bi bi-instagram"></i>
                        <p>Instagram</p>
                        </a>
                        </div>
                        <div class="col text-center">
                        <a class="text-decoration-none text-dark" href="#">
                        <i class="bi bi-whatsapp"></i>
                        <p>Whatsapp</p>
                        </a>
                        </div>
                        <div class="col text-center">
                        <a class="text-decoration-none text-dark" href="#">
                        <i class="bi bi-telephone-fill"></i>
                        <p>+88-02-000000000</p>
                        </a>
                        </div>
                        
                    </div>
                </div>              
            </div>

        HEADER;
        echo $header;
    ?>


</body>
</html>