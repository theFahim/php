<?php

    $students = [
        'cse' => [
            ['name' => 'Himel','email' => 'himel@gmail.com','result' => 4],
            ['name' => 'Zarin','email' => 'zarin@gmail.com','result' => 4],
        ],
        'bba' => [
            ['name' => 'Tamim','email' => 'tamim@gmail.com','result' => 4],
            ['name' => 'Kafi','email' => 'kafim@gmail.com','result' => 3.9],
        ],
        'eee' => [
            ['name' => 'Tanvir','email' => 'tanvir@gmail.com','result' => 4],
            ['name' => 'Rafi','email' => 'kafim@gmail.com','result' => 3.5],
        ],
    ];
    // echo "<pre>";
    // var_dump($students);
    // echo "</pre>";
    
    // echo "Department: CSE <br>Students:<br>";
    // for ($i=0; $i < count($students['cse']); $i++) { 
    //     # code...
    //     echo "Name :"." ". $students['cse'][$i]['name'].", ";
    //     echo "Email :"." ". $students['cse'][$i]['email'].", ";
    //     echo "Result :"." ". $students['cse'][$i]['result'].",";
    //     echo "<br>";
    // }
    // echo "<br>";
    // echo "Department: BBA <br>Students:<br>";
    
    // for ($i=0; $i < count($students['cse']); $i++) { 
    //     # code...
    //     echo "Name :"." ". $students['bba'][$i]['name'].", ";
    //     echo "Email :"." ". $students['bba'][$i]['email'].", ";
    //     echo "Result :"." ". $students['bba'][$i]['result'].",";
    //     echo "<br>";
    // }

    
    foreach ($students as $key => $value) {
        # code...
        echo "Department: ".strtoupper($key)."<br>";
        echo "Students: <br>";
        foreach ($value as $k => $v) {
            # code...
            echo $k+1 .". Name: ".$v["name"];
            echo " Email: ".$v["email"];
            echo " Result: ".$v["result"]." <br>";
        }
        echo "<br> <br>";
    }

?>