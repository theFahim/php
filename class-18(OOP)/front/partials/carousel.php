    <!-- Carousel -->
    <div id="demo" class="carousel slide" data-bs-ride="carousel">

    <!-- Indicators/dots -->
    <div class="carousel-indicators">
      <?php
      $counter = 0;
      $active = 'active';
      foreach ($slides as $key => $value):
      
      ?>
      <button type="button" data-bs-target="#demo" data-bs-slide-to="<?=$counter?>" class="<?=$active?>"></button>

      <?php
      $counter++;
      $active='';
      endforeach;
      ?>

    </div>
    
    <!-- The slideshow/carousel -->
    <div class="carousel-inner">
      <?php
      $active = 'active';
      foreach ($slides as $key => $value):
      ?>
      <div class="carousel-item <?=$active?>">
        <img src="img/<?= $value['picture']?>" alt="<?=$value['captiontitle']?>" class="d-block" style="width:100%">
        <div class="carousel-caption">
          <h3><?=$value['caption']?></h3>
          <p><?=$value['captiontitle']?></p>
        </div>
      </div>
      <?php
      $active = '';
      endforeach;
      ?>
    </div>

    <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
        <span class="carousel-control-next-icon"></span>
      </button>
    </div>