<?php 
  include_once($_SERVER['DOCUMENT_ROOT']."/SEIP-B5-Fahim/PHP/Config.php");

  use Pondit\Sliders\Slider; 
  use Pondit\Utility\Utility;
  $utility = new Utility();
  $objSlider = new Slider();
  // var_dump($objSlider);
  $slides = $objSlider->all();

  $utility->d($slides);


?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  </head>
  <body>
  <?php include_once($_SERVER['DOCUMENT_ROOT']."/SEIP-B5-Fahim/PHP/class-18(OOP)/front/partials/carousel.php")?>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
  </body>
</html>