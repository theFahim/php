<?php

include_once("vendor/autoload.php");

use Pondit\Students\Person;
use Pondit\Courses\Course; 
    
    $niloy = new Person();
    $niloy->name = 'Niloy';
    // $niloy->sayHello();
    $niloy->sayName();

    $php = new Course();
    $php->title = 'PHP & Laravel';

    echo $niloy->sayName()." is a Student of Course ".$php->title;
?>