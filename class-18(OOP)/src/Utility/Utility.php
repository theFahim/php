<?php 

namespace Pondit\Utility;

class Utility{

    public function debug($data){
        echo'<pre>';
        print_r($data);
        echo'</pre>';
        echo '<hr>';
    }

    public function d($data){
        $this->debug($data);
    }

    public function dd($data){
        $this->debug($data);
        die(__FILE__.__LINE__);
    }

    public function redirect($url){
        header("location:$url");
    }
}