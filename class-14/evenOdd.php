<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>oven or odd</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  </head>
  <body>
    
        <div class="container text-center my-5">
            <form method="get">
            <label for="exampleDataList" class="form-label">Enter any Number to check if it is even or odd:</label>
            <input type="number" class="form-control " list="datalistOptions" id="exampleDataList" name="checkif" placeholder="Enter any number">
            <button class="btn btn-outline-primary my-2">Check</button>
            </form>

            <?php

            if (isset($_GET['checkif'])) {
                # code...
                $value = $_GET['checkif'];
                if ($value%2==0) {
                    # code...
                    echo 'Even';
                }
                else {
                    # code...
                    echo "Odd";
                }
            }


        ?>
        </div>




  


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
  </body>
</html>