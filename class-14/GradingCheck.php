<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Grading check</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  </head>
  <body>
    
        <div class="container text-center my-5">
            <form method="get">
            <label for="exampleDataList" class="form-label">Check the grading of the student by Entering His/Her Score:</label>
            <input type="number" class="form-control " list="datalistOptions" id="exampleDataList" name="checkif" placeholder="Enter any number">
            <button class="btn btn-outline-primary my-2">Check</button>
            </form>

            <?php

            if (isset($_GET['checkif'])) {
                # code...
                $grading = $_GET['checkif'];
                if ($grading > 100  || $grading < 0) {
                  echo "Invalid Input";
              }
              else {
                  # code...
                  switch ((int)($grading/10) ) {
              
                      case 10:
                      case 9:
                      case 8:
                          # code...
                          echo "A+";
                          break;
                      case 7:
                          # code...
                          echo "A";
                          break;
                      case 6:
                          # code...
                          echo "A-";
                          break;
                      case 5:
                          # code...
                          echo "B";
                          break;
                      case 4:
                          # code...
                          echo "C";
                          break;
                      
                      default:
                          # code...
                          echo "F";
                          break;
                  }
              }
            }


        ?>
        </div>




  


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
  </body>
</html>