    <!-- Footer -->
    <footer class="bg-dark text-center text-white">
        <!-- Grid container -->
        <div class="container p-4">
        <!-- Section: Social media -->
        <section class="mb-4">
            <!-- Facebook -->
            <a class="btn btn-outline-primary btn-floating m-1 text-white" href="#!" role="button"
            ><i class="fab fa-facebook-f"></i
            ></a>
    
            <!-- Twitter -->
            <a class="btn btn-outline-primary btn-floating m-1 text-white" href="#!" role="button"
            ><i class="fab fa-twitter"></i
            ></a>
    
            <!-- Google -->
            <a class="btn btn-outline-primary btn-floating m-1 text-white" href="#!" role="button"
            ><i class="fab fa-google"></i
            ></a>
    
            <!-- Instagram -->
            <a class="btn btn-outline-primary btn-floating m-1 text-white" href="#!" role="button"
            ><i class="fab fa-instagram"></i
            ></a>
    
            <!-- Linkedin -->
            <a class="btn btn-outline-primary btn-floating m-1 text-white" href="#!" role="button"
            ><i class="fab fa-linkedin-in"></i
            ></a>
    
            <!-- Github -->
            <a class="btn btn-outline-primary btn-floating m-1 text-white" href="#!" role="button"
            ><i class="fab fa-gitlab"></i
            ></a>
        </section>
        <!-- Section: Social media -->
    
        <!-- Section: Form -->
        <section class="">
            <form action="">
            <!--Grid row-->
            <div class="row d-flex justify-content-center">
                <!--Grid column-->
                <div class="col-auto">
                <p class="pt-2">
                    <strong>Sign up for our newsletter</strong>
                </p>
                </div>
                <!--Grid column-->
    
                <!--Grid column-->
                <div class="col-md-5 col-12">
                <!-- Email input -->
                <div class="form-outline form-white mb-4">
                    <input type="email" id="form5Example21" class="form-control" />
                    <label class="form-label" for="form5Example21">Email address</label>
                </div>
                </div>
                <!--Grid column-->
    
                <!--Grid column-->
                <div class="col-auto">
                <!-- Submit button -->
                <button type="submit" class="btn btn-outline-light mb-4">
                    Subscribe
                </button>
                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->
            </form>
        </section>
        <!-- Section: Form -->
    

    
        <!-- Section: Links -->
        <section class="">
            <!--Grid row-->
            <div class="row">
            <!--Grid column-->
            <div class="col-lg-4 col-md-6 mb-4 mb-md-0">
                <ul class="list-unstyled mb-0">
                <li>
                    <a href="#!" class="text-white text-decoration-none">Privacy Policy</a>
                </li>
                <li>
                    <a href="#!" class="text-white text-decoration-none">Brands</a>
                </li>
                <li>
                    <a href="#!" class="text-white text-decoration-none">About Us</a>
                </li>
                <li>
                    <a href="#!" class="text-white text-decoration-none">Terms and Condition</a>
                </li>
                </ul>
            </div>
            <!--Grid column-->

    
            <!--Grid column-->
            <div class="col-lg-4 col-md-6 mb-4 mb-md-0">
    
                <ul class="list-unstyled  mb-0">
                <li>
                    <a href="#!" class="text-white text-decoration-none">Blog</a>
                </li>
                <li>
                    <a href="#!" class="text-white text-decoration-none">Online Delivery</a>
                </li>
                <li>
                    <a href="#!" class="text-white text-decoration-none">Refund and Return Policy</a>
                </li>
                <li>
                    <a href="#!" class="text-white text-decoration-none">Contact Us</a>
                </li>
                </ul>
            </div>
            <!--Grid column-->
    
            <!--Grid column-->
            <div class="col-lg-4 col-md-6 mb-4 mb-md-0">
    
                <ul class="list-unstyled mb-0">
                <li>
                    <p><i class="bi bi-telephone">&nbsp;&nbsp;019000000000</i></p>
                </li>
                <li>
                    <a href="#!" class="text-white text-decoration-none"><i class="bi bi-geo-alt-fill">Find our stores</i></a>
                </li>

                </ul>
            </div>
            <!--Grid column-->
            </div>
            <!--Grid row-->
        </section>
        <!-- Section: Links -->
        </div>
        <!-- Grid container -->
    
        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2022 Copyright:
        <a class="text-white" href="home.html">thegadgetzone.com</a>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- Footer -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>