<?php include_once 'header.php' ?>

    <!-- Carousel -->
    <div class="container ml-5 mt-3 justify-content-center">
        <!-- Carousel -->
    <div id="demo" class="carousel slide" data-bs-ride="carousel">

      <!-- Indicators/dots -->
      <div class="carousel-indicators">
        <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
        <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
        <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
        <button type="button" data-bs-target="#demo" data-bs-slide-to="3"></button>
      </div>
      
      <!-- The slideshow/carousel -->
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="./images/discount.png" alt="discount" class="d-block" style="width:100%">
        </div>
        <div class="carousel-item">
          <img src="https://wallpaperaccess.com/full/1912279.jpg" alt="Los Angeles" class="d-block" style="width:100%">
          <div class="carousel-caption">
            <h3>CPU</h3>
            <p>Vital Part</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="https://wallpaperaccess.com/full/3379059.jpg" alt="Chicago" class="d-block" style="width:100%">
          <div class="carousel-caption">
            <h3>Motherboard</h3>
            <p>Holds Everything Togather</p>
          </div> 
        </div>
        <div class="carousel-item">
          <img src="https://wallpaperaccess.com/full/2325987.jpg" alt="New York" class="d-block" style="width:100%">
          <div class="carousel-caption">
            <h3>GPU</h3>
            <p>Helps the CPU calculate more</p>
          </div>  
        </div>
      </div>

      <!-- Featured Product -->
      



      <!-- Left and right controls/icons -->
      <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
        <span class="carousel-control-next-icon"></span>
      </button>
    </div>
    </div>

    <!-- Featured categories -->

    <div class="container py-5 text-center">
        <h3>Featured categories</h3>
        <p>Get Your Desired Product from Featured Category!</p>

        <div class="container pt-5">
            <div class="row">
                <!-- laptop icon -->

                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="laptop.html" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/laptop-48x48.png" alt="laptop">
                    <p class="icon-link">Laptop</p>
                    </a>
                </div>
                <!-- desktop icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="desktop.html" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/desktop-48x48.png" alt="Desktop">
                    <p class="icon-link">Desktop</p>
                    </a>
                </div>
                <!-- Processor icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/cpu-48x48.png" alt="Processor">
                    <p class="icon-link">Processor</p>
                    </a>
                </div>
                <!-- Graphics icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/gpu-48x48.png" alt="Graphics">
                    <p class="icon-link">Graphics Card</p>
                    </a>
                </div>
                <!-- SSD icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/SSD-48x48.png" alt="SSD">
                    <p class="icon-link">SSD</p>
                    </a>
                </div>
                <!-- Keyboard icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/keyboard-48x48.png" alt="Keyboard">
                    <p class="icon-link">Keyboard</p>
                    </a>
                </div>
                <!-- Mouse icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/accessories-48x48.png" alt="laptop">
                    <p class="icon-link">Mouse</p>
                    </a>
                </div>
                <!-- Headphone Icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/headphone-48x48.png" alt="laptop">
                    <p class="icon-link">Headphone</p>
                    </a>
                </div>
            </div>
            <div class="row py-5">
                <!-- webcam icon -->

                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/webcam-48x48.png" alt="webcam">
                    <p class="icon-link">Webcam</p>
                    </a>
                </div>
                <!-- Printer icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/printer-48x48.png" alt="Printer">
                    <p class="icon-link">Printer</p>
                    </a>
                </div>
                <!-- Projector icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/projector-48x48.png" alt="Projector">
                    <p class="icon-link">Projector</p>
                    </a>
                </div>
                <!-- Router icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/router-48x48.png" alt="Router">
                    <p class="icon-link">Router</p>
                    </a>
                </div>
                <!-- Studio Equipment icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/studio-equipment-48x48-48x48.png" alt="studio">
                    <p class="icon-link">Studio Equipment</p>
                    </a>
                </div>
                <!-- Gadget icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="accessories.html" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/gadget-48x48.png" alt="Gadget">
                    <p class="icon-link">Gadget</p>
                    </a>
                </div>
                <!-- Smart watch icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/smart-watch-48x48.png" alt="watch">
                    <p class="icon-link">Smart Watch</p>
                    </a>
                </div>
                <!-- Converter and cable Icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.startech.com.bd/image/cache/catalog/category-thumb/cable-converter-48x48.png" alt="converter">
                    <p class="icon-link">Converter & cable</p>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- Featured Product -->
    <div class="container py-5 text-center">
        <h3>Featured Product</h3>
        <p>Check & Get Your Desired Product !</p>
    </div>

    <!-- Featured Product List -->
    <section>
        <div class="container py-5">
          <div class="row py-3">
              <!-- product 1 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                    <div class="card">
                      <img src="https://www.startech.com.bd/image/cache/catalog/mouse/razer/deathadder-essetial/deathadder-essential-500x500.jpg" width="296px" height="296px"
                        class="card-img-top" alt="Laptop" />
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                          <p class="small text-danger"><s>&#2547;1900</s></p>
                        </div>
            
                        <div class="d-flex justify-content-between mb-3">
                          <h5 class="mb-0">Razer DeathAdder Essential Gaming Mouse</h5>
                          <h5 class="text-dark mb-0">&#2547;1699</h5>
                        </div>
            
                        <div class="d-flex justify-content-between mb-2">
                          <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                          <div class="ms-auto text-warning">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                          </div>
                          <div class="d-flex flex-column mt-5">
                            <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

              <!-- product 2 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Laptop</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 3 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/razer/blade-15-advanced-model/blade-15-advanced-model-01-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Laptop</a></p>
                      <p class="small text-danger"><s>&#2547;365,000</s></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Razer Blade 15 i7 11th Gen 32GB RAM RTX3080</h5>
                      <h5 class="text-dark mb-0">&#2547;360,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 4 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/dell/15-3500/vostro-15-3500-silver-front-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Laptop</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Dell Vostro 15 3500 Core i3 11th Gen 15.6" FHD </h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
            <div class="row py-3">
              <!-- product 1 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                    <div class="card">
                      <img src="https://www.startech.com.bd/image/cache/catalog/monitor/benq/xl2546k/zowie-xl2546-228x228.jpg" width="296px" height="296px"
                        class="card-img-top" alt="Laptop" />
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <p class="small"><a href="#!" class="text-muted">Monitor</a></p>
                          <p class="small text-danger"><s>&#2547;</s></p>
                        </div>
            
                        <div class="d-flex justify-content-between mb-3">
                          <h5 class="mb-0">BenQ ZOWIE XL2546 24.5 inch FHD 240Hz</h5>
                          <h5 class="text-dark mb-0">&#2547;53,500</h5>
                        </div>
            
                        <div class="d-flex justify-content-between mb-2">
                          <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                          <div class="ms-auto text-warning">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                          </div>
                          <div class="d-flex flex-column mt-5">
                            <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

              <!-- product 2 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/monitor/samsung/c49g95tssw/c49g95tssw-1-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Monitor</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Samsung Odyssey 49'' G-Sync 240Hz Curved 2k Gaming Monitor
                    </h5>
                      <h5 class="text-dark mb-0">&#2547;184,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 3 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/motherboard/asrock/b660m-phantom-gaming-4/b660m-phantom-gaming-4-01-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Components</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">ASRock B660M Phantom Gaming 4 12th Gen Micro ATX Motherboard</h5>
                      <h5 class="text-dark mb-0">&#2547;13,600</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 4 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/monitor/asus/proart-pa278qv/proart-pa278qv-1-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Monitor</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">ASUS ProArt PA278QV 27-inch WQHD Monitor
                    </h5>
                      <h5 class="text-dark mb-0">&#2547;49,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
            <div class="row py-3">
              <!-- product 1 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                    <div class="card">
                      <img src="https://www.startech.com.bd/image/cache/catalog/headphone/razer/kraken-x/kraken-x-1-500x500.jpg" width="296px" height="296px"
                        class="card-img-top" alt="Laptop" />
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <p class="small"><a href="#!" class="text-muted">Accessories</a></p>
                          <p class="small text-danger"><s>$1900</s></p>
                        </div>
            
                        <div class="d-flex justify-content-between mb-3">
                          <h5 class="mb-0">Razer DeathAdder Essential Gaming Mouse</h5>
                          <h5 class="text-dark mb-0">$1699</h5>
                        </div>
            
                        <div class="d-flex justify-content-between mb-2">
                          <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                          <div class="ms-auto text-warning">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                          </div>
                          <div class="d-flex flex-column mt-5">
                            <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

              <!-- product 2 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">$105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 3 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 4 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
            <div class="row py-3">
              <!-- product 1 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                    <div class="card">
                      <img src="https://www.startech.com.bd/image/cache/catalog/mouse/razer/deathadder-essetial/deathadder-essential-500x500.jpg" width="296px" height="296px"
                        class="card-img-top" alt="Laptop" />
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                          <p class="small text-danger"><s>&#2547;1900</s></p>
                        </div>
            
                        <div class="d-flex justify-content-between mb-3">
                          <h5 class="mb-0">Razer DeathAdder Essential Gaming Mouse</h5>
                          <h5 class="text-dark mb-0">&#2547;1699</h5>
                        </div>
            
                        <div class="d-flex justify-content-between mb-2">
                          <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                          <div class="ms-auto text-warning">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                          </div>
                          <div class="d-flex flex-column mt-5">
                            <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

              <!-- product 2 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 3 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 4 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
      



        </div>
    </section>

    <!-- Newsletter -->

    <section class="container-fluid mt-3 mb-5">
        <form action="">
        <!--Grid row-->
        <div class="row d-flex justify-content-center bg-dark text-center text-white pt-5">
            <!--Grid column-->
            <div class="col-auto">
            <p class="pt-2">
                <strong>Sign up for More New Product</strong>
            </p>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-5 col-12">
            <!-- Email input -->
            <div class="form-outline form-white mb-4">
                <input type="email" id="form5Example21" class="form-control" />
                <label class="form-label" for="form5Example21">Email address</label>
            </div>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-auto">
            <!-- Submit button -->
            <button type="submit" class="btn btn-outline-light mb-4">
                Subscribe
            </button>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
        </form>
    </section>

    <!-- New Arrivals -->
    <div class="container py-5 text-center">
        <h3>New Arrivals</h3>
        <p>Check & Get Your Newest Desired Product !</p>
    </div>
    <!-- New Arrival Product List -->
    <section>
        <div class="container py-5">
          <div class="row py-3">
              <!-- product 1 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                    <div class="card">
                      <img src="https://www.startech.com.bd/image/cache/catalog/mouse/razer/deathadder-essetial/deathadder-essential-500x500.jpg" width="296px" height="296px"
                        class="card-img-top" alt="Laptop" />
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                          <p class="small text-danger"><s>&#2547;1900</s></p>
                        </div>
            
                        <div class="d-flex justify-content-between mb-3">
                          <h5 class="mb-0">Razer DeathAdder Essential Gaming Mouse</h5>
                          <h5 class="text-dark mb-0">&#2547;1699</h5>
                        </div>
            
                        <div class="d-flex justify-content-between mb-2">
                          <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                          <div class="ms-auto text-warning">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                          </div>
                          <div class="d-flex flex-column mt-5">
                            <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

              <!-- product 2 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 3 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 4 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
            <div class="row py-3">
              <!-- product 1 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                    <div class="card">
                      <img src="https://www.startech.com.bd/image/cache/catalog/mouse/razer/deathadder-essetial/deathadder-essential-500x500.jpg" width="296px" height="296px"
                        class="card-img-top" alt="Laptop" />
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                          <p class="small text-danger"><s>&#2547;1900</s></p>
                        </div>
            
                        <div class="d-flex justify-content-between mb-3">
                          <h5 class="mb-0">Razer DeathAdder Essential Gaming Mouse</h5>
                          <h5 class="text-dark mb-0">&#2547;1699</h5>
                        </div>
            
                        <div class="d-flex justify-content-between mb-2">
                          <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                          <div class="ms-auto text-warning">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                          </div>
                          <div class="d-flex flex-column mt-5">
                            <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

              <!-- product 2 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 3 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 4 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
            <div class="row py-3">
              <!-- product 1 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                    <div class="card">
                      <img src="https://www.startech.com.bd/image/cache/catalog/mouse/razer/deathadder-essetial/deathadder-essential-500x500.jpg" width="296px" height="296px"
                        class="card-img-top" alt="Laptop" />
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                          <p class="small text-danger"><s>&#2547;1900</s></p>
                        </div>
            
                        <div class="d-flex justify-content-between mb-3">
                          <h5 class="mb-0">Razer DeathAdder Essential Gaming Mouse</h5>
                          <h5 class="text-dark mb-0">&#2547;1699</h5>
                        </div>
            
                        <div class="d-flex justify-content-between mb-2">
                          <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                          <div class="ms-auto text-warning">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                          </div>
                          <div class="d-flex flex-column mt-5">
                            <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

              <!-- product 2 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 3 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 4 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
            <div class="row py-3">
              <!-- product 1 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                    <div class="card">
                      <img src="https://www.startech.com.bd/image/cache/catalog/mouse/razer/deathadder-essetial/deathadder-essential-500x500.jpg" width="296px" height="296px"
                        class="card-img-top" alt="Laptop" />
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                          <p class="small text-danger"><s>&#2547;1900</s></p>
                        </div>
            
                        <div class="d-flex justify-content-between mb-3">
                          <h5 class="mb-0">Razer DeathAdder Essential Gaming Mouse</h5>
                          <h5 class="text-dark mb-0">&#2547;1699</h5>
                        </div>
            
                        <div class="d-flex justify-content-between mb-2">
                          <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                          <div class="ms-auto text-warning">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                          </div>
                          <div class="d-flex flex-column mt-5">
                            <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

              <!-- product 2 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 3 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              <!-- product 4 -->
              <div class="col-md-3 col-lg-3 mb-4 mb-lg-0">
                <div class="card">
                  <img src="https://www.startech.com.bd/image/cache/catalog/laptop/apple/macbook-air/MGN73/macbook-mgn73Zp-a-500x500.jpg" width="296px" height="296px"
                    class="card-img-top" alt="Laptop" />
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="small"><a href="#!" class="text-muted">Mouse</a></p>
                    </div>
        
                    <div class="d-flex justify-content-between mb-3">
                      <h5 class="mb-0">Apple MacBook Air 13.3-Inch Retina Display M1 chip</h5>
                      <h5 class="text-dark mb-0">&#2547;105,000</h5>
                    </div>
        
                    <div class="d-flex justify-content-between mb-2">
                      <p class="text-muted mb-0">Available: <span class="fw-bold">6</span></p>
                      <div class="ms-auto text-warning">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <div class="d-flex flex-column mt-5">
                        <a href="product-details.html" class="btn btn-info" role="button">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
      



        </div>
    </section>

    <!-- Brand Products -->
    <div class="container py-5 text-center">
        <h3>Brand Products</h3>
        <p>Check & Browse Your Favorite Brand Products!</p>

        <div class="container pt-5">
            <div class="row brand">
                <!-- laptop icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://wallpaperaccess.com/full/112314.jpg" alt="laptop" width="200px" height="112px">
                    <p class="icon-link">Micro-Star International</p>
                    </a>
                </div>
                <!-- desktop icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://c4.wallpaperflare.com/wallpaper/1020/142/556/5bd268b2cb20b-wallpaper-preview.jpg">
                    <p class="icon-link">Hewlett Packard</p>
                    </a>
                </div>
                <!-- Processor icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://wallpaperaccess.com/full/3743780.jpg">
                    <p class="icon-link">Lenovo</p>
                    </a>
                </div>
                <!-- Graphics icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://wallpaperaccess.com/full/6376579.jpg" alt="Graphics">
                    <p class="icon-link">Gigabyte</p>
                    </a>
                </div>
                <!-- intel icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.wallpaperflare.com/static/570/21/93/intel-logo-wallpaper.jpg" alt="SSD">
                    <p class="icon-link">Intel</p>
                    </a>
                </div>
                <!-- Keyboard icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.amd.com/system/files/2020-06/amd-default-social-image-1200x628.jpg" alt="Keyboard">
                    <p class="icon-link">AMD</p>
                    </a>
                </div>
                <!-- Mouse icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://wallpaperaccess.com/full/81635.jpg" alt="laptop">
                    <p class="icon-link">Asus</p>
                    </a>
                </div>
                <!-- Headphone Icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://www.nvidia.com/content/dam/en-zz/Solutions/about-nvidia/logo-and-brand/02-nvidia-logo-color-grn-500x200-4c25-p@2x.png" alt="laptop">
                    <p class="icon-link">Nvidia</p>
                    </a>
                </div>
                <!-- Headphone Icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://logodix.com/logo/2094748.png" alt="laptop">
                    <p class="icon-link">Asrock</p>
                    </a>
                </div>
                <!-- Headphone Icon -->
                <div class="col-md col-lg mb-4 mb-lg-0 d-block">
                    <a href="#" class="text-decoration-none text-black">
                    <img src="https://logos-world.net/wp-content/uploads/2020/11/Razer-Symbol.jpg" alt="laptop">
                    <p class="icon-link">Razer</p>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include_once 'footer.php' ?>