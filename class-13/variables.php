<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>variables</title>
</head>
<body>
    <?php
        //data types
        $name = 'Fahim'; //string
        $age = 25; //integer
        $isMale = true; //boolean
        $height = 1.67; //float
        $salary = null; // null

        echo $name.'<br>';
        echo $age.'<br>';
        echo $isMale.'<br>';
        echo $height.'<br>';
        echo $salary.'<br>';

        echo gettype($name).'<br>';
        echo gettype($age).'<br>';
        echo gettype($isMale).'<br>';
        echo gettype($height).'<br>';
        echo gettype($salary).'<br>';

        var_dump($name,$age,$isMale,$height,$salary);

        echo '<br>';

        echo is_string($name).'<br>';
        echo is_int($age).'<br>';
        echo is_bool($isMale).'<br>';
        echo is_double($height).'<br>';

        echo isset($name).'<br>';
        echo isset($address).'<br>';

        //constants
        define('pi',3.1416);
        echo pi.'<br>';

        //PHP build in constants
        echo PHP_INT_MAX.'<br>';

        // case-insensitive constant name
        // define("GREETING", "Welcome to W3Schools.com!", true);
        // echo greeting;

        //array constant
        define("cars", [
            "Alfa Romeo",
            "BMW",
            "Toyota"
          ]);
          echo cars[0];
    ?>
     
</body>
</html>