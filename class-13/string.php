<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Strings</title>
</head>
<body>
    <?php
    $name = 'fahim';
    $string = 'Hello i am $name. am 25';
    $string2 = "hello i am $name i am 25";
    echo $string.'<br>';
    echo $string2.'<br>';
    //string concatenation
    echo 'hello'.'World'.'and'.'php'.'<br>';

    // String functions
    $string = "    Hello World      ";

    echo "1 - " . strlen($string) . '<br>'; 
    echo "2 - " . trim($string) . '<br>';
    echo "3 - " . ltrim($string) . '<br>';
    echo "4 - " . rtrim($string) . '<br>' ;
    echo "5 - " . str_word_count($string) . '<br>' ;
    echo "6 - " . strrev($string) . '<br>' ;
    echo "7 - " . strtoupper($string) . '<br>';
    echo "8 - " . strtolower($string) . '<br>';
    echo "9 - " . ucfirst('hello') . '<br>' ;
    echo "10 - " . lcfirst('HELLO') . '<br>' ;
    echo "11 - " . ucwords('hello world') . '<br>';
    echo "12 - " . strpos($string, 'world') . '<br>';
    echo "13 - " . stripos($string, 'world') . '<br>';
    echo "14 - " . substr($string, 8) . '<br>';
    echo "15 - " . str_replace('World', 'PHP', $string) . '<br>' ;
    echo "16 - " . str_ireplace('world', 'PHP', $string) . '<br>';

    // Multiline text and line breaks
    $longText = "
    Hello, my name is Fahim
    I am 25,
    I love my daughter
    ";
    echo $longText . '<br>'; 
    echo nl2br($longText) . '<br>'; 

    // Multiline text and reserve html tags
    $longText = "
    Hello, my name is <b>Fahim</b>
    I am <b>25</b>,
    I love my daughter
    ";
    echo "1 - " . $longText . '<br>';
    echo "2 - " . nl2br($longText) . '<br>';
    echo "3 - " . htmlentities($longText) . '<br>'; 
    echo "4 - " . nl2br(htmlentities($longText)) . '<br>'; 
    echo "5 - " . html_entity_decode('&lt;b&gt;Fahim&lt;/b&gt;') . '<br>';
    echo "6 - " . htmlspecialchars($longText) . '<br>'; 


    ?>
</body>
</html>