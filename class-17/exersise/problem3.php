<?php
    // Problem 3
    
    
    $person = array(
        "Sophia"=>"31",
        "Jacob"=>"41",
        "William"=>"39",
        "Ramesh"=>"40"
    );


    //sort by value acending order
    asort($person);
    echo "<pre>";
    print_r($person);
    echo "</pre>";

    //sort by value decending order

    arsort($person);
    echo "<pre>";
    print_r($person);
    echo "</pre>";

    //sort by key acending order
    ksort($person);
    echo "<pre>";
    print_r($person);
    echo "</pre>";

    //sort by key decending order

    krsort($person);
    echo "<pre>";
    print_r($person);
    echo "</pre>";


?>